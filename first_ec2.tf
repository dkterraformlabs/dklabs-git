

    resource "aws_db_instance" "default" {
      allocated_storage    = 20
      storage_type         = "gp2"
      engine               = "mysql"
      engine_version       = "5.7"
      instance_class       = "db.t2.micro"
      name                 = "mydb"
      username             = "foo"
      password             = "foobarbaz"
      parameter_group_name = "default.mysql5.7"
      publicly_accessible  = "false"
      skip_final_snapshot = "true"
    }



    # Terraform log settings on windows for current session and files
#$env:TF_LOG="TRACE"
#$env:TF_LOG_PATH="terraform.txt"

/*
resource "aws_eip" "lb" {
vpc      = true
}

output "eip" {
value = aws_eip.lb.public_ip
}

resource "aws_s3_bucket" "mys3" {
bucket = "dklabs-attribute-demo1"
}

output "mys3bucket" {
value = aws_s3_bucket.mys3.bucket_domain_name
}

resource "aws_instance" "myec2dk" {
    ami = "ami-04d5cc9b88f9d1d39"
    #instance_type = "t2.large"
    instance_type = var.instance_type
    #sky = "blue"
  }


resource "aws_db_instance" "default" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "foo"
  password             = "foobarbaz"
  parameter_group_name = "default.mysql5.7"
  publicly_accessible  = "false"
  skip_final_snapshot = "true"
}

resource "aws_eip" "lb" {
  vpc      = true
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.myec2.id
  allocation_id = aws_eip.lb.id
}


resource "aws_security_group" "allow_tls" {
  name        = "kplabs-security-group"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${aws_eip.lb.public_ip}/32"]

#    cidr_blocks = [aws_eip.lb.public_ip/32]
  }
}
*/
